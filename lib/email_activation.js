var _    = require('lodash');
var log  = require('./logger').getLogger('email_activation');
var Q    = require('q');

var Hashids = require('hashids');
// WARNING!!! Do NOT change hashids salt in production!
var hashids = new Hashids("hive email activation");

var User    = require('./user');
var errors  = require('./errors');
var mailer  = require('./mailer');

var EmailActivator = {};

/**
 * Create email activation link from user _id (hash with salt).
 * This link has no expiration date. It will send a new email
 * each time it called with the same activation link.
 * @param {object} user - User to activate email.
 * @returns {Promise|*} Promise - Sent mail.
 */
EmailActivator.createActivationLink = function(user) {
    var data = {
        emailActivationLink: hashids.encryptHex(String(user._id)),
        user: user
    };
    return mailer.activateEmail(user.email, data, user.lang);
};

/**
 * Activate email by link (get user _id from link).
 * @param {string} hash - Email activation link.
 * @returns {Promise|*} Promise - User entity.
 */
EmailActivator.activateEmail = function(hash) {
    var userId = hashids.decryptHex(hash);
    return User.getOne({_id: userId})
        .then(function(user) {
            if(!user) {
                log.error('user not found for email activation');
                return Q.reject(errors.NO_USER_TO_ACTIVATE_EMAIL());
            }
            user.nae = undefined;
            return user.save();
        });
};

module.exports = EmailActivator;
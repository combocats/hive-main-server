var log = require("./logger").getLogger('api_user');
var _   = require("lodash");

var express = require("express");
var app = module.exports = express();

var User = require('./user');
var Country = require('./country');
var useragent = require('useragent');
var passport = require('passport');

var mailer = require('./mailer');
var EmailChanger = require('./email_change');
var validator = require('./validate_api_user');

/**
 * Create new user from data in request body.
 */
app.post('/users', validator.postUsers, function(req, res, next) {
    log.debug("post /users", req.body);

    log.info("post /users ips, ip, country:", req.headers['x-real-ip']);
    req.body.country = Country.getByIp(req.headers['x-real-ip']);

    var agent = useragent.parse(req.headers['user-agent']);
    req.body.agent = agent.os.toString();

    User.create(req.body).then(
        function(user) {
            req.logIn(user, function(err) {
                if(err) {
                    log.debug("user login after registration faild", err);
                    return next(errors.USER_LOGIN_FAILD());
                }
                res.data = {accessToken: req.sessionID};
                return next();
            });
        },
        function(err) {
            next(err);
        }
    );
});

/**
 * Change password of current logged user to new if old one is correct.
 */
app.post('/change/pwd', validator.postChangePwd, passport.ensureAuthenticated, function(req, res, next) {
    log.debug("post /change/pwd", req.body);

    User.getOne({_id: req.user})
        .then(function(user) {
            log.info('fetch user from session id', user);
            return user;
        })
        .then(function(user) {
            return user.changePassword(req.body.current, req.body.password);
        })
        .then(function(user) {
            log.debug("successfully change password", user);
            mailer.passwordChanged(user.email, {user: user}, user.lang);

            res.data = {};
            return next();
        }, function(err) {
            return next(err);
        });
});

/**
 * Create link for changing email. Body should have
 * `email` and `password` properties.
 */
app.post('/change/email', validator.postChangeEmail,  passport.ensureAuthenticated, function(req, res, next) {
    log.debug("post /change/email ", req.body);

    User.getOne({_id: req.user})
        .then(function(user) {
            return EmailChanger.createChangeLink(user, req.body.email, req.body.password);
        })
        .then(function() {
            var data = {};
            return next();
        }, function(err) {
            return next(err);
        });
});

/**
 * Get current logged user.
 */
app.get('/users', passport.ensureAuthenticated, function(req, res, next) {
    log.debug("get /users", req.user);

    User.getOne({_id: req.user}).then(
        function(user) {
            res.data = user;
            next();
        },
        function(err) {
            next(err);
        }
    );
});

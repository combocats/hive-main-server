var V = require('./validate');

var Validator = {};

Validator.postUsers = function(req, res, next) {
    V.chain([
        V.pickOnly(req, 'body', ['email', 'password']),
        V.checkTypes(req.body, {email: String, password: String}),
        V.mustHaveKeys(req.body, ['email', 'password']),
        V.password(req.body.password),
        V.sanitize(req.body, ['email', 'password'])
    ], next);
};

Validator.postChangePwd = function(req, res, next) {
    V.chain([
        V.pickOnly(req, 'body', ['current', 'password']),
        V.checkTypes(req.body, {current: String, password: String}),
        V.mustHaveKeys(req.body, ['current', 'password']),
        V.password(req.body.password),
        V.sanitize(req.body, ['current', 'password'])
    ], next);
};

/**
 * For now it is the same as for postUsers
 * @type {Function}
 */
Validator.postChangeEmail = Validator.postUsers;

module.exports = Validator;
/*
 * Errors with password recover starts from 11***
 */
module.exports = {
    HASH_LINK_EXISTS: {
        status: 409,
        code: 11001,
        message: 'Hahs link already exists, check your email'
    },
    HASH_LINK_CREATE: {
        status: 500,
        code: 11002,
        message: 'Error on creating hash link'
    },
    GET_USER_ID_FROM_HASH_LINK: {
        status: 500,
        code: 11003,
        message: 'Error on getting user from hash link'
    },
    REMOVE_HASH_LINK: {
        status: 500,
        code: 11004,
        message: 'Error on removing hash link'
    },
    RECOVER_EMAIL_NOT_FOUND: {
        status: 404,
        code: 11005,
        message: 'User with such email could not be found'
    },
    HASH_LINK_NOT_FOUND: {
        status: 404,
        code: 11006,
        message: 'Hash link not found'
    },
    NO_USER_FOR_HASH_LINK: {
        status: 404,
        code: 11007,
        message: 'Hash link does not match any user'
    }
};
var Q       = require("q");
var _       = require("lodash");
var log     = require("./logger").getLogger('mailer');
var swig    = require("swig");
var emailjs = require("emailjs");
var path    = require("path");
var errors  = require("./errors");
var settings = require("./settings");

//TODO call with mail_server config object
var mailServer = emailjs.server.connect(settings.config.mail_server);

/* Get all predefined emails */
var emails = require('./emails');

/**
 * Compile and render specified template.
 * @param template - Name of the template to render.
 * @param data - Object with locals.
 * @param lang - 2-chars language code (default to 'en').
 * @returns {string} - Rendered output.
 */
function renderTemplate(template, data, lang) {
    var pathToTemplate = path.resolve(__dirname, "templates/email/" + lang + "/" + template);

    return Q.nbind(swig.renderFile, swig)(pathToTemplate, data)
        .catch(function(err) {
            log.error("error on rendering email template", err);
            return Q.reject(errors.EMAIL_RENDER());
        })
}

/**
 * Create email Object with predefined structure.
 * @param to - Email of recipient.
 * @param subject - Subject of email.
 * @param html - HTML version of email.
 * @param text - Text version of email
 * @returns {{text: *, from: string, to: *, subject: *, attachments: Array}} - Ready-to-use email for emailjs module.
 */
function createEmail(to, subject, html, text) {
    return {
        text: text,
        from: "Hive <hive@hive.com>", //TODO get this field from config?
        to: to,
        subject: subject,
        attachments: [
            {
                data: html,
                alternative:true
            }
        ]
    }
}

/**
 * Send email to recipient.
 * @param mail - emailjs email object
 * @returns {Q.catch|*} - Promise.
 */
function sendEmail(mail) {
    return Q.nbind(mailServer.send, mailServer)(mail)
        .then(function(sentMail) {
            log.debug("email successfully sent", sentMail);
            return sentMail;
        })
        .catch(function(err) {
            log.error("could not send email", {err: err, mail:mail});
            return Q.reject(errors.COULD_NOT_SEND_EMAIL({err: err}));
        });
}

var mailer = {
    sendEmail: sendEmail
};

/* Create functions from emails Object */
_.forEach(emails, function(value, key) {
    /**
     * Shorthand function for sending emails.
     * @param to - Email of recipient.
     * @param data - Object with locals.
     * @param lang - 2-chars language code.
     * @returns {*|Q.spread}
     */
    mailer[key] = function(to, data, lang) {
        lang = lang || 'en';
        data.host = settings.config.host;

        return Q.all([
                renderTemplate(value.html, data, lang),
                renderTemplate(value.text, data, lang)
            ]).spread(function(html, text) {
                var email = createEmail(to, value.subject[lang], html, text);
                return sendEmail(email);
            });
    };
});

module.exports = mailer;

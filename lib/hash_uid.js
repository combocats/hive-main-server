var Hashids = require('hashids');
var salt = "some salt";
var minHashLength = 6;
var alphabet = "123456789ABCDEFGHJKMNPQRSTUVWXYZ";

var hashids = new Hashids(salt, minHashLength, alphabet);

var hashUid = {};

/**
 * Get 6 chars length hash from specified id.
 * @param id {Number} - Uid to get hash of.
 * @returns {String} - 6 chars length hash.
 */
hashUid.getHash = function(id) {
    return hashids.encrypt(id);
};

/**
 * Get original uid from specified hash.
 * @param hash {String} - Hash to get uid of.
 * @returns {Number} - Original uid.
 */
hashUid.getIdFromHash = function(hash) {
    return hashids.decrypt(hash)[0];
};

module.exports = hashUid;
var log = require('./logger').getLogger('api_pwdrecover');
var Q   = require('q');

var express = require("express");
var app = module.exports = express();

var Pwdrecover = require('./pwdrecover');

var Validator  = require('./validate_api_auth');

/*
 * Create recover link and send email to specified address.
 */
app.post('/pwdrecover', Validator.postPwdrecover, function(req, res, next) {
    log.debug("post /pwdrecover", req.body);

    Pwdrecover.createRecoverLink(req.body.email)
        .then(function(data) {
            res.data = data;
            return next();
        }, function(err) {
           return next(err);
        });
});

/*
 * Reset password according to recover link and new password.
 */
app.post('/pwdreset', Validator.postPwdreset, function(req, res, next) {
    log.debug("post /pwdreset", req.body);

    Pwdrecover.resetPassword(req.body)
        .then(function(data) {
            res.data = {};
            return next();
        }, function(err) {
            return next(err);
        })
});

/*
 * Errors with mailer starts from 20***
 */
module.exports = {
    EMAIL_RENDER: {
        status: 500,
        code: 20001,
        message: 'Could not prepare email'
    },
    COULD_NOT_SEND_EMAIL: {
        status: 500,
        code: 20002,
        message: 'Could not send email'
    }
};
var settings = require('./settings');
var mongoose = require('mongoose');
var Schema   = mongoose.Schema;

var User = new Schema({
    uid: {
        type: Number,
        required: true,
        index: {
            unique: true
        }
    },
    name: {
        type: String
    },
    email: {
        type: String
    },
    // not activated email address
    nae: {
        type: Boolean
    },
    active: {
        type: Boolean
    },
    friends: [{
        type: Schema.Types.ObjectId
    }],
    /* fields for analytics */
    country: {
        type: String
    },
    agent: {
        type: String
    },
    created: {
        type: Date,
        required: true,
        default: Date.now
    },
    /* credentials for local authorization strategy */
    salt: {
        type: String
    },
    hashpass: {
        type: String
    }
}, {
    versionKey: false
});

var db_uri = settings.config.db_host + settings.config.db_name;
var userDBConnection = mongoose.createConnection(db_uri);
module.exports = userDBConnection.model('user', User);
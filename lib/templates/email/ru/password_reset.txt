{% extends './base_email.txt' %}

{% block mailbody %}
Вы установили новый пароль используя ссылку для васстановления.

Если у вас есть вопросы, или вы не восстаналивали пароль, свяжитесь с нами support@hive.com.
{% endblock %}
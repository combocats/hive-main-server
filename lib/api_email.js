var log = require("./logger").getLogger('api_email');

var express = require("express");
var app = module.exports = express();

var EmailActivator = require('./email_activation');
var EmailChanger   = require('./email_change');

/**
 * Activate email by specified link param.
 */
app.get('/email/activate/:link', function(req, res, next) {
    log.debug("get /email/activate/:link", req.param('link'));

    EmailActivator.activateEmail(req.param('link'))
        .then(function(user) {
            log.debug('email successfully activated', user.email);
            return res.redirect('/emailactivated');
        })
        .catch(function(err) {
            return res.redirect('/404');
        });
});

/**
 * Complete email changing by specified link param.
 */
app.get('/email/change/:link', function(req, res, next) {
    log.debug("get /email/change/:link", req.param('link'));

    EmailChanger.changeEmail(req.param('link'))
        .then(function(user) {
            log.debug("email successfully changed", user.email);
            return res.redirect('/emailchanged');
        })
        .catch(function(err) {
            return res.redirect('/404');
        });
});

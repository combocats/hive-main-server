_ = require('lodash');

module.exports = {
    /**
     * Create mongodb query with $or in odred to find duplicates.
     * @param data - Any mongodb Model data
     * @returns {{$or: Array}} - Ready to use mongo query
     */
    makeDuplicatesQuery: function(data) {
        var keys = _.toArray(arguments).slice(1);
        var uniqueFields = _.pick(data, keys);

        var query = {
            $or: []
        };

        _.forEach(uniqueFields, function(value, key) {
            var i = {};
            i[key] = value;
            query.$or.push(i);
        });

        return query;
    }
};
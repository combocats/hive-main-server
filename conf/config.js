/**
 * Example configurations for development and for production.
 * This config file will be changed while deploying, so use it
 * just like an example or doc.
 */
module.exports = {
    dev: {
        port: 3003,
        host: "http://localhost:3003",
        db_host: 'mongodb://localhost/',
        db_name: 'hive-dev',
        mail_server: {
            port: 4011, // should be more then 1024 for tests!
            output: '../logs/smtp', // where should smtp mock put .eml files
            from: "mail@hive.com"
        },
        redis_port: '6379',
        redis_host: 'localhost',
        // password recovering
        hash_link_expire: 1000, // seconds
        session_max_age: 1000 * 60 * 60 * 24
    },
    prod: {

    },
    test: {
        port: 9000,
        host: "http://localhost:3003",
        db_host: 'mongodb://localhost/',
        db_name: 'hive-test',
        mail_server: {
            port: 4013, // should be more then 1024 for tests!
            output: './logs/smtp', // where should smtp mock put .eml files
            from: "mail@hive.com"
        },
        redis_port: '6379',
        redis_host: 'localhost',
        // password recovering
        hash_link_expire: 10, // seconds
        session_max_age: 1000 * 60
    },
    unittest: {
        db_uri: 'mongodb://localhost/hive-test',
        redis_port: '6379',
        redis_host: 'localhost',
        // password recovering
        hash_link_expire: 10 // seconds
    }
};
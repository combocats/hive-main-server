var hooks = require('../e2e_hooks');
require('should');
var Modules = require('../modules');

var request = require('request');

var headers = {
    'Content-Type': 'application/json',
    'user-agent': 'Mozilla/5.0 (iPhone; CPU iPhone OS 6_1 like Mac OS X) AppleWebKit/536.26 (KHTML, like Gecko) Version/6.0 Mobile/10B141 Safari/8536.25'
};

var config = require(Modules.settings).config;

function url(url) {
    return "http://localhost:" + config.port + "/api" + url;
}

describe('lib.api_auth', function() {

    before(hooks.before);
    after(hooks.after);

    describe('Local login with email/password', function() {

        it('should create test user with password', function(done) {
            var user = {
                name: "zahar",
                email: "zahar@lodossteam.com",
                password: "qwerty123"
            };
            request({
                url: url("/users"),
                method: "POST",
                headers: headers,
                body: JSON.stringify(user)
            }, function(err, res, body) {
                res.statusCode.should.be.equal(200);
                done();
            });
        });

        it('should login created user', function(done) {
            var data = {
                email: "zahar@lodossteam.com",
                password: "qwerty123"
            };
            request({
                url: url("/auth/local"),
                method: "POST",
                headers: headers,
                body: JSON.stringify(data)
            }, function(err, res, body) {
                res.statusCode.should.be.equal(200);
                body = JSON.parse(body);
                body.should.have.keys(['accessToken']);
                done();
            });
        });

        it('should return 403 status and 10005 code for non existed email', function(done) {
            var data = {
                email: "wrong@mail.com",
                password: "qwerty123"
            };
            request({
                url: url("/auth/local"),
                method: "POST",
                headers: headers,
                body: JSON.stringify(data)
            }, function(err, res, body) {
                res.statusCode.should.be.equal(403);
                body = JSON.parse(body);
                body.code.should.be.equal(10005);
                done();
            });
        });

        it('should return 403 status and 10006 code for not valid password', function(done) {
            var data = {
                email: "zahar@lodossteam.com",
                password: "wrongpassword"
            };
            request({
                url: url("/auth/local"),
                method: "POST",
                headers: headers,
                body: JSON.stringify(data)
            }, function(err, res, body) {
                res.statusCode.should.be.equal(403);
                body = JSON.parse(body);
                body.code.should.be.equal(10006);
                done();
            });
        });

    });

});
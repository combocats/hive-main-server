var hooks = require('../e2e_hooks');
require('should');
var Modules = require('../modules');
var errors_recoverhash = require(Modules.errors_hashlink);

var request = require('request');

var headers = {
    'Content-Type': 'application/json',
    'user-agent': 'Mozilla/5.0 (iPhone; CPU iPhone OS 6_1 like Mac OS X) AppleWebKit/536.26 (KHTML, like Gecko) Version/6.0 Mobile/10B141 Safari/8536.25'
};

var config = require(Modules.settings).config;

function url(url) {
    return "http://localhost:" + config.port + "/api" + url;
}

describe('lib.api_pwdrecover', function() {

    before(hooks.before);
    after(hooks.after);

    describe('Local login with email/password', function() {

        it('should create test user with password', function(done) {
            var user = {
                name: "zahar",
                email: "zahar@lodossteam.com",
                password: "qwerty123"
            };
            request({
                url: url("/users"),
                method: "POST",
                headers: headers,
                body: JSON.stringify(user)
            }, function(err, res, body) {
                res.statusCode.should.be.equal(200);
                done();
            });
        });

    });

    describe('Create recover letter', function() {

        it('should send recover letter to client', function(done) {
            var data = {
                email: "zahar@lodossteam.com"
            };
            request({
                url: url("/pwdrecover"),
                method: "POST",
                headers: headers,
                body: JSON.stringify(data)
            }, function(err, res, body) {
                res.statusCode.should.be.equal(200);
                done();
            });
        });

        it('should return error on second attempt to create recover link', function(done) {
            var data = {
                email: "zahar@lodossteam.com"
            };
            request({
                url: url("/pwdrecover"),
                method: "POST",
                headers: headers,
                body: JSON.stringify(data)
            }, function(err, res, body) {
                res.statusCode.should.be.equal(errors_recoverhash.HASH_LINK_EXISTS.status);
                body = JSON.parse(body);
                body.code.should.be.equal(errors_recoverhash.HASH_LINK_EXISTS.code);
                done();
            });
        });

        it('should return error for not existed user', function(done) {
            var data = {
                email: "fake@lodossteam.com"
            };
            request({
                url: url("/pwdrecover"),
                method: "POST",
                headers: headers,
                body: JSON.stringify(data)
            }, function(err, res, body) {
                res.statusCode.should.be.equal(errors_recoverhash.RECOVER_EMAIL_NOT_FOUND.status);
                body = JSON.parse(body);
                body.code.should.be.equal(errors_recoverhash.RECOVER_EMAIL_NOT_FOUND.code);
                done();
            });
        });

    });
});
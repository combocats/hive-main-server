var path = require('path');
var _ = require('lodash');

var Modules = {
    'user'              : './lib/user',
    'MongoUser'         : './lib/db_mongo_user',
    'APIUser'           : './lib/api_user',
    'settings'          : './lib/settings',
    'hashUid'           : './lib/hash_uid',
    'uid'               : './lib/uid',
    'mongoUtils'        : './lib/mongo_utils',
    'errors'            : './lib/errors',
    'errors_user'       : './lib/errors_user',
    "errors_hashlink"   : './lib/errors_hashlink',
    'country'           : './lib/country',
    'mailer'            : './lib/mailer',
    'hashlink'          : './lib/hashlink',
    'pwdrecover'        : './lib/pwdrecover',
    'email_activation'  : './lib/email_activation',
    'email_change'      : './lib/email_change',
    'validate'          : './lib/validate'
};

var absolutPaths = {};
_.forEach(Modules, function(value, key) {
    absolutPaths[key] = path.resolve(value);
});

module.exports = absolutPaths;
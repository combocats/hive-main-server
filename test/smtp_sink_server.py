from datetime import datetime
import asyncore
import os
from smtpd import SMTPServer
import argparse

parser = argparse.ArgumentParser(description='Port argument.')
parser.add_argument('--port', dest='port', default=4011,
                   help='smtp port')
parser.add_argument('--output', dest='output', default='./logs/smtp',
                   help='smtp output')

args = parser.parse_args()

class EmlServer(SMTPServer):
    no = 0
    def process_message(self, peer, mailfrom, rcpttos, data):
        directory = args.output
        if not os.path.exists(directory):
            os.makedirs(directory)
        filename = '%s/%s-%d.eml' % (directory, datetime.now().strftime('%Y%m%d%H%M%S'),
                self.no)
        f = open(filename, 'w')
        f.write(data)
        f.close
        print '%s saved.' % filename
        self.no += 1


def run():
    foo = EmlServer(('localhost', int(args.port)), None)
    try:
        asyncore.loop()
    except KeyboardInterrupt:
        pass


if __name__ == '__main__':
	run()
process.env.NODE_ENV = 'unittest';
require('should');
var Q = require('q');
var _ = require('lodash');
var proxyquire =  require('proxyquire');

var Modules = require('../modules');
var errors = require(Modules.errors);

describe('lib.pwdrecover', function() {

    describe('Pwdrecover.createRecoverLink', function() {

        it('should create recover link and return ok', function(done) {
            var Pwdrecover = proxyquire(Modules.pwdrecover, {
                './user': {
                    getOne: function(email) {
                        return Q({email: email, name: email, _id: 111});
                    }
                },
                './hashlink': {
                    create: function(id) {
                        return Q("recoverHash:" + id);
                    }
                },
                './mailer': {
                    passwordRecover: function() {
                        return Q(true);
                    }
                }
            });

            Pwdrecover.createRecoverLink("zahar@lodossteam.com")
                .then(function(res) {
                    should.be.ok;
                }, function(err) {
                    'error'.should.be.equal('success');
                }).done(done);
        });

        it('should return error on second attempt', function(done) {
            var Pwdrecover = proxyquire(Modules.pwdrecover, {
                './user': {
                    getOne: function(email) {
                        return Q({email: email, name: email, _id: 111});
                    }
                },
                './hashlink': {
                    create: function(id) {
                        return Q.reject(errors.HASH_LINK_EXISTS());
                    }
                }
            });

            Pwdrecover.createRecoverLink("zahar@lodossteam.com")
                .then(function(res) {
                    'success'.should.be.equal('error');
                }, function(err) {
                    err.should.be.ok;
                    err.code.should.be.equal(errors.HASH_LINK_EXISTS().code);
                }).done(done);
        });

        it('should return error when send email fails', function(done) {
            var Pwdrecover = proxyquire(Modules.pwdrecover, {
                './user': {
                    getOne: function(email) {
                        return Q({email: email, name: email, _id: 111});
                    }
                },
                './hashlink': {
                    create: function(id) {
                        return Q("recoverHahs:" + id);
                    }
                },
                './mailer': {
                    passwordRecover: function() {
                        return Q.reject(errors.COULD_NOT_SEND_EMAIL());
                    }
                }
            });

            Pwdrecover.createRecoverLink("zahar@lodossteam.com")
                .then(function(res) {
                    'success'.should.be.equal('error');
                }, function(err) {
                    err.should.be.ok;
                    err.code.should.be.equal(errors.COULD_NOT_SEND_EMAIL().code);
                }).done(done);
        });

    });

    describe('Pwdrecover.resetPassword', function() {

        it('should successfully reset password', function(done) {
            var Pwdrecover = proxyquire(Modules.pwdrecover, {
                './user': {
                    getOne: function() {
                        var User = function() {
                            _.extend(this, {email: 'zahar@lodossteam.com', name: 'Zahar A.', _id: "some_user_id"});
                        };
                        User.prototype.setEncryptedPassword = function(password) {
                            this.password = password;
                            return Q(this);
                        };
                        User.prototype.save = function() {
                            return Q(this);
                        };
                        return Q(new User());
                    }
                },
                './hashlink': {
                    get: function() {
                        return Q("some_user_id");
                    },
                    remove: function(link) {
                        return Q(true);
                    }
                }
            });

            Pwdrecover.resetPassword({link: "some_recover_link", password: "qwerty123"})
                .then(function(user) {
                    should.be.ok;
                    user.should.have.properties([
                        'email',
                        '_id',
                        'password'
                    ]);
                }, function(err) {
                    'error'.should.be.equal('success');
                }).done(done);
        });

        it('should return error if there is no user for recover link', function(done) {
            var Pwdrecover = proxyquire(Modules.pwdrecover, {
                './user': {
                    getOne: function() {
                        return Q(null);
                    }
                },
                './hashlink': {
                    get: function() {
                        return Q("some_user_id");
                    }
                }
            });

            Pwdrecover.resetPassword({link: "some_recover_link", password: "qwerty123"})
                .then(function(user) {
                    'success'.should.be.equal('error');
                }, function(err) {
                    err.code.should.be.equal(errors.NO_USER_FOR_HASH_LINK().code);
                }).done(done);
        });

    });

});
process.env.NODE_ENV = 'unittest';
require('should');
var Q = require('q');
var _ = require('lodash');
var proxyquire =  require('proxyquire');

var Modules = require('../modules');
var errors  = require(Modules.errors);

describe('lib.email_activation', function() {

    describe('EmailActivator.createActivationLink', function() {

        var EmailActivator = proxyquire(Modules.email_activation, {
            './mailer': {
                activateEmail: function(to, data) {
                    return Q(data);
                }
            }
        });

        it('should create activation link and send email to user', function(done) {
            var user = {
                _id: '528cad6552064f426fee196b',
                email: 'zahar@lodossteam.com'
            };

            EmailActivator.createActivationLink(user)
                .then(function(data) {
                    data.should.have.properties([
                        'emailActivationLink',
                        'user'
                    ]);
                    data.emailActivationLink.length.should.be.equal(20);
                }, function(err) {
                    'error'.should.be.equal('success');
                }).done(done);
        });

    });

    describe('EmailActivator.activateEmail', function() {

        it('should remove nae from user entity and save it', function(done) {
            var EmailActivator = proxyquire(Modules.email_activation, {
                './user': {
                    getOne: function(query) {
                        var User = function() {
                            _.extend(this, {email: 'zahar@lodossteam.com', nae: true, name: 'Zahar A.'}, query);
                        };
                        User.prototype.save = function() {
                            return Q(this);
                        };
                        return Q(new User());
                    }
                }
            });

            var emailActivationLink = 'B3GJAOB4GdhkMzV2pK1A';

            EmailActivator.activateEmail(emailActivationLink)
                .then(function(data) {
                    data.should.not.have.property('nae');
                    data._id.length.should.be.equal(24);
                }, function(err) {
                    'error'.should.be.equal('success');
                }).done(done);
        });

        it('should remove nae from user entity and save it', function(done) {
            var EmailActivator = proxyquire(Modules.email_activation, {
                './user': {
                    getOne: function() {
                        return Q(null);
                    }
                }
            });

            var emailActivationLink = 'BjzDpyY7zAi1gzLvAbnr';

            EmailActivator.activateEmail(emailActivationLink)
                .then(function(data) {
                    'success'.should.be.equal('error');
                }, function(err) {
                    err.code.should.be.equal(errors.NO_USER_TO_ACTIVATE_EMAIL().code);
                }).done(done);
        });

    });

});

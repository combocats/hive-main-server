process.env.NODE_ENV = 'unittest';
require('should');
var _       = require('lodash');
var Q       = require('q');
var sinon   = require('sinon');
var Modules = require('../modules');
var errors  = require(Modules.errors);
var proxyquire =  require('proxyquire');


describe('lib.hashlink', function() {

    describe('Hashlink.create', function() {

        it('should properly create hash and store it in redis', function(done) {
            var HashLink = proxyquire(Modules.hashlink, {
                'redis': {
                    createClient: function() {
                        return {
                            get: function() {
                                return _.last(arguments)(null, null);
                            },
                            multi: function() {
                                return {
                                    set: function() {},
                                    expire: function() {},
                                    exec: function() {
                                        return _.last(arguments)(null, ['ok']);
                                    }
                                }
                            }
                        }
                    }
                },
                'crypto': {
                    randomBytes: function() {
                        return "some_random_hash";
                    }
                }
            });

            HashLink.create('some_user_id').then(function(data) {
                data.should.be.equal('some_random_hash');
            }, function(err) {
                'error'.should.equal('success');
            }).done(done);
        });

        it('should properly create hash with data payload and store it in redis', function(done) {

            var payload;
            var HashLink = proxyquire(Modules.hashlink, {
                'redis': {
                    createClient: function() {
                        return {
                            get: function() {
                                return _.last(arguments)(null, null);
                            },
                            multi: function() {
                                return {
                                    set: function(key, data) {
                                        payload = data;
                                    },
                                    expire: function() {},
                                    exec: function() {
                                        return _.last(arguments)(null, ['ok']);
                                    }
                                }
                            }
                        }
                    }
                },
                'crypto': {
                    randomBytes: function() {
                        return "some_random_hash";
                    }
                }
            });

            HashLink.create('some_user_id', {email: 'zahar@lodossteam.com'}).then(function(data) {
                payload = JSON.parse(payload);
                payload.email.should.be.equal('zahar@lodossteam.com');
                payload.id.should.be.equal('some_user_id');
            }, function(err) {
                'error'.should.equal('success');
            }).done(done);
        });

        it('should return error on second attempt to create same link', function(done) {
            var HashLink = proxyquire(Modules.hashlink, {
                'redis': {
                    createClient: function() {
                        return {
                            get: function() {
                                return _.last(arguments)(null, "some_random_hash");
                            }
                        }
                    }
                }
            });

            HashLink.create('some_user_id').then(function(id) {
                'success'.should.equal('error');
            }, function(err) {
                err.should.have.properties([
                    'status',
                    'code',
                    'message'
                ]);
                err.code.should.be.equal(errors.HASH_LINK_EXISTS().code);
            }).done(done);
        });

        it('should return error on creating hash (redis fails)', function(done) {
            var HashLink = proxyquire(Modules.hashlink, {
                'redis': {
                    createClient: function() {
                        return {
                            get: function() {
                                return _.last(arguments)(null, null);
                            },
                            multi: function() {
                                return {
                                    set: function() {},
                                    expire: function() {},
                                    exec: function() {
                                        return _.last(arguments)({error: 'error'});
                                    }
                                }
                            }
                        }
                    }
                }
            });

            HashLink.create('some_user_id').then(function(id) {
                'success'.should.equal('error');
            }, function(err) {
                err.should.have.properties([
                    'status',
                    'code',
                    'message'
                ]);
                err.code.should.be.equal(errors.HASH_LINK_CREATE().code);
            }).done(done);
        });

    });

    describe('Hashlink.get', function() {

        it('should fetch user id from redis by hash', function(done) {
            var HashLink = proxyquire(Modules.hashlink, {
                'redis': {
                    createClient: function() {
                        return {
                            get: function() {
                                return _.last(arguments)(null, 'some_user_id');
                            }
                        }
                    }
                }
            });


            HashLink.get('some_hash_link').then(function(id) {
                id.should.be.equal('some_user_id');
            }, function(err) {
                'error'.should.equal('success');
            }).done(done);
        });

        it('should return error on fetching from db', function(done) {
            var HashLink = proxyquire(Modules.hashlink, {
                'redis': {
                    createClient: function() {
                        return {
                            get: function() {
                                return _.last(arguments)({error: 'error'});
                            }
                        }
                    }
                }
            });

            HashLink.get('some_random_hash').then(function(id) {
                'success'.should.equal('error');
            }, function(err) {
                err.should.have.properties([
                    'status',
                    'code',
                    'message'
                ]);
                err.code.should.be.equal(errors.GET_USER_ID_FROM_HASH_LINK().code);
            }).done(done);
        });

        it('should return error if there is no such link', function(done) {
            var HashLink = proxyquire(Modules.hashlink, {
                'redis': {
                    createClient: function() {
                        return {
                            get: function() {
                                return _.last(arguments)(null, null);
                            }
                        }
                    }
                }
            });

            HashLink.get('some_random_hash').then(function(id) {
                'success'.should.equal('error');
            }, function(err) {
                err.should.have.properties([
                    'status',
                    'code',
                    'message'
                ]);
                err.code.should.be.equal(errors.HASH_LINK_NOT_FOUND().code);
            }).done(done);
        });

    });

    describe('Hashlink.remove', function() {

        it('should remove id->link and link->id relations from db', function(done) {
            var HashLink = proxyquire(Modules.hashlink, {
                'redis': {
                    createClient: function() {
                        return {
                            multi: function() {
                                return {
                                    get: function() {
                                        return _.last(arguments)(null, 'some_user_id');
                                    },
                                    del: function() {},
                                    exec: function() {
                                        return _.last(arguments)(null, ['ok']);
                                    }
                                }
                            }
                        }
                    }
                }
            });

            HashLink.remove('some_random_hash').then(function() {
                should.be.ok;
            }, function(err) {
                'error'.should.equal('success');
            }).done(done);
        });

        it('should return error when redis get() fails', function(done) {
            var HashLink = proxyquire(Modules.hashlink, {
                'redis': {
                    createClient: function() {
                        return {
                            multi: function() {
                                return {
                                    get: function() {
                                        return _.last(arguments)({error: 'error'});
                                    },
                                    del: function() {},
                                    exec: function() {
                                        return _.last(arguments)(null, null);
                                    }
                                }
                            }
                        }
                    }
                }
            });

            HashLink.remove('some_random_hash').then(function() {
                'success'.should.equal('error');
            }, function(err) {
                err.should.have.properties([
                    'status',
                    'code',
                    'message'
                ]);
                err.code.should.be.equal(errors.REMOVE_HASH_LINK().code);
            }).done(done);
        });

        it('should return error when redis exec() fails', function(done) {
            var HashLink = proxyquire(Modules.hashlink, {
                'redis': {
                    createClient: function() {
                        return {
                            multi: function() {
                                return {
                                    get: function() {
                                        return _.last(arguments)(null, 'some_user_id');
                                    },
                                    del: function() {},
                                    exec: function() {
                                        return _.last(arguments)({error: 'error'});
                                    }
                                }
                            }
                        }
                    }
                }
            });

            HashLink.remove('some_random_hash').then(function() {
                'success'.should.equal('error');
            }, function(err) {
                err.should.have.properties([
                    'status',
                    'code',
                    'message'
                ]);
                err.code.should.be.equal(errors.REMOVE_HASH_LINK().code);
            }).done(done);
        });

    });

});
var settings = require('./lib/settings');
var log      = require('./lib/logger').getLogger('app');
var _        = require('lodash');

var express  = require('express');

var app = express();

app.use(express.urlencoded());
app.use(express.json());
app.use(express.cookieParser());
app.use(express.methodOverride());

/* Config session storage */
var RedisStore = require('connect-redis')(express);
var redis = require('redis');
var sessionClient = redis.createClient(settings.config.redis_port, settings.config.redis_host);

var sessionConf = {
    secret: 'hive secret cat',
    cookie: {
        maxAge: settings.config.session_max_age
    },
    store: new RedisStore({client: sessionClient})
};

//CORS middleware for
app.use(function(req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
    res.header('Access-Control-Allow-Headers', 'Content-Type');

    next();
});

// token based authorization for CORS and non-cookie agents
app.use(function(req, res, next) {
    if(!req.cookies['connect.sid']) {
        req.cookies['connect.sid'] = req.param('accessToken');
    }
    next();
});

app.use(express.session(sessionConf));

/**
 * Mount all API modules using app.use method.
 * Here we can add any API prefix we need.
 */
app.use('/api', require('./lib/api_auth'));
app.use('/api', require('./lib/api_user'));
app.use('/api', require('./lib/api_pwdrecover'));

app.use(require('./lib/api_email'));

app.get('/', function(req, res) {
    log.debug("get main page");
    res.json({ok: true});
});

/**
 * End point for all error requests to public API
 */
app.use('/api', function(err, req, res, next) {
    log.error('api error', {error: err});
    res.status(err.status || 500);
    res.json(err);
});

/**
 * End point for all non error requests to api
 */
app.use('/api', function(req, res) {
    log.debug('end api call by sending request', {data: res.data});
    if(_.isNull(res.data)) {
        res.status(404);
        return res.json();
    }
    if(_.isUndefined(res.data)) {
        return res.json();
    }
    res.json(res.data);
});

app.listen(settings.config.port);
log.info("listen on %s", settings.config.port);
